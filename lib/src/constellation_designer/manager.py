from __future__ import annotations

import datetime as dt
import logging
import os
from pathlib import Path

import attr
from ccsds_ndm.ndm_io import NDMFileFormats, NdmIo

from .core import Constellation
from .omm import ndm_from_constellation

logger = logging.getLogger(__name__)


def generate_basename(constellation, format, date=None):
    if not date:
        date = dt.datetime.now()

    basename = (
        "constellation_"
        f"{constellation.num_satellites}_{constellation.num_planes}"
        f"_{format}_{date.strftime('%Y-%m-%dT%H_%M_%S')}"
    )
    return basename


@attr.s(auto_attribs=True)
class ConstellationManager:
    constellation: Constellation | None = attr.ib(default=None)
    exports_dir: str = attr.ib(default="output")

    @exports_dir.validator
    def exports_dir_exists(self, attribute, value):
        if not os.path.isdir(value):
            raise ValueError("exports directory does not exist")

    def export_omm(self):
        if self.constellation is None:
            raise ValueError("Constellation is not set")

        logger.info(
            "Exporting constellation of %d satellites to OMM",
            len(self.constellation.satellites),
        )

        ndm = ndm_from_constellation(self.constellation)

        filename = f"{generate_basename(self.constellation, 'omm')}.xml"
        export_path = Path(self.exports_dir) / filename

        NdmIo().to_file(
            ndm,
            NDMFileFormats.XML,
            export_path,
            no_namespace_schema_location=(
                "http://cwe.ccsds.org/moims/docs/MOIMS-NAV/Schemas/"
                "ndmxml-1.0-master.xsd"
            ),
        )

        logger.info("File exported to %s", export_path)

        return export_path

    def export_tle(self):
        if self.constellation is None:
            raise ValueError("Constellation is not set")

        logger.info(
            "Exporting constellation of %d satellites to TLE",
            len(self.constellation.satellites),
        )
