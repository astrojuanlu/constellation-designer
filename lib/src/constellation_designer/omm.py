from decimal import Decimal

from astropy import units as u
from ccsds_ndm.models.ndmxml2 import (AngleType, BStarType, DdRevType,
                                      DRevType, InclinationType,
                                      MeanElementsType, Ndm, NdmHeader,
                                      OmmBody, OmmData, OmmSegment, OmmType,
                                      RevType, TleParametersType)
from poliastro.twobody import Orbit
from poliastro.twobody.angles import E_to_M, nu_to_E

from ._version import __version__
from .core import Constellation, orbit_from_satellite


def omm_from_orbit(orbit: Orbit, num_decimals=16):
    mean_motion = round(Decimal(orbit.n.to_value(u.deg / u.day)), num_decimals)
    inclination = round(Decimal(orbit.inc.to_value(u.deg)), num_decimals)
    ra_of_asc_node = round(Decimal(orbit.raan.to_value(u.deg)), num_decimals)
    arg_of_pericenter = round(Decimal(orbit.argp.to_value(u.deg)), num_decimals)
    mean_anomaly = round(
        Decimal(E_to_M(nu_to_E(orbit.nu, orbit.ecc), orbit.ecc).to_value(u.deg)),
        num_decimals,
    )

    mean_elements = MeanElementsType(
        epoch=orbit.epoch.isot + "Z",
        mean_motion=RevType(
            value=mean_motion,
            units=None,
        ),
        eccentricity=Decimal(orbit.ecc.value),
        inclination=InclinationType(value=inclination, units=None),
        ra_of_asc_node=AngleType(value=ra_of_asc_node, units=None),
        arg_of_pericenter=AngleType(value=arg_of_pericenter, units=None),
        mean_anomaly=AngleType(
            value=mean_anomaly,
            units=None,
        ),
    )
    tle_parameters = TleParametersType(
        ephemeris_type=0,
        classification_type="U",
        norad_cat_id=99999,
        element_set_no=999,
        rev_at_epoch=0,
        bstar=BStarType(value=Decimal("0.0"), units=None),
        mean_motion_dot=DRevType(value=Decimal("0.0"), units=None),
        mean_motion_ddot=DdRevType(value=Decimal("0"), units=None),
    )

    omm = OmmType(
        header=NdmHeader(
            creation_date=orbit.epoch.isot + "Z",
            originator=f"Constellation Designer, version {__version__}",
        ),
        body=OmmBody(
            segment=OmmSegment(
                data=OmmData(
                    mean_elements=mean_elements,
                    tle_parameters=tle_parameters,
                    covariance_matrix=None,
                    user_defined_parameters=None,
                ),
            ),
        ),
    )
    return omm


def ndm_from_constellation(constellation: Constellation):
    omms = []
    for satellite in constellation.satellites:
        omms.append(omm_from_orbit(orbit_from_satellite(satellite)))

    ndm = Ndm(omm=omms)
    return ndm
