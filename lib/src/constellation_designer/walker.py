import numpy as np
from sympy import Eq, cos, cot, csc, nsolve, pi, symbols

from .core import InvalidConstellationError, Plane, Satellite


def walker_star(n, p):
    """Walker star constellation.

    Parameters
    ----------
    n : int
        Number of satellites per plane.
    p : int
        Total number of planes.

    References
    ----------
    .. [walker1970] Walker, J. G. (1970). _Circular orbit patterns
       providing continuous whole earth coverage_.
       Royal Aircraft Establishment Farnborough (United Kingdom).

    """
    α = symbols("α", real=True)
    n_, p_ = symbols("n, p", natural=True)

    alpha_eq3 = Eq(
        cos(pi / n_) * csc(2 * α) - cot(2 * α), cos(pi / (2 * n_)) * cot((p_ - 1) * α)
    )
    alpha_eq4 = Eq(cos(pi / n_) * csc(2 * α) - cot(2 * α), cot((p_ - 1) * α))

    if n < 1:
        raise InvalidConstellationError("Use at least 1 satellite")

    if (p % 2 == 0 and n % 2 == 0) or (p % 2 == 1 and n % 2 == 1):
        # Solve for α from appropriate equation
        if p < 2:
            raise InvalidConstellationError("Use at least 2 planes")
        elif p == 2:
            alpha = (
                np.arccos(
                    (np.cos(np.pi / n) - np.cos(np.pi / (2 * n)))
                    / (1 + np.cos(np.pi / (2 * n)))
                )
                / 2
            )
        elif p == 3:
            alpha = np.arccos(np.cos(np.pi / n) / (1 + np.cos(np.pi / (2 * n)))) / 2
        else:
            alpha = float(
                nsolve(
                    (alpha_eq3.rhs - alpha_eq3.lhs).subs({p_: p, n_: n}),
                    α,
                    cos(pi / 3) / 2,
                )
            )
        # Solve for β from equation 1
        beta = np.pi / 2 - (p - 1) * alpha
        # Compute d_max using equation 3
        d_max = np.arccos(
            np.cos(np.pi / n)
            * np.cos(np.arctan(np.cos(np.pi / (2 * n)) * np.tan(beta)))
        )
    else:
        # Solve for α from appropriate equation
        if p < 2:
            raise InvalidConstellationError("Use at least 2 planes")
        elif p == 2:
            alpha = np.arccos((np.cos(np.pi / n) - 1) / 2) / 2
        elif p == 3:
            alpha = np.arccos(np.cos(np.pi / n) / 2) / 2
        else:
            alpha = float(
                nsolve(
                    (alpha_eq4.rhs - alpha_eq4.lhs).subs({p_: p, n_: n}),
                    α,
                    cos(pi / 3) / 2,
                )
            )
        # Solve for β from equation 1
        beta = np.pi / 2 - (p - 1) * alpha
        # Compute d_max using equation 4
        d_max = np.arccos(np.cos(np.pi / n) * np.cos(beta))

    return d_max, alpha, beta


def walker_star_satellites(n, p):
    _, alpha, beta = walker_star(n, p)
    for ii in range(p):
        plane = Plane(ii, np.pi / 2, beta + 2 * ii * alpha)
        for jj in range(n):
            yield Satellite(plane, jj, jj * 2 * np.pi / n)
