from __future__ import annotations

from collections import namedtuple

import attr
from astropy import units as u
from astropy.time import Time
from poliastro.bodies import Earth
from poliastro.plotting import OrbitPlotter3D
from poliastro.twobody import Orbit


class InvalidConstellationError(ValueError):
    pass


Plane = namedtuple("Plane", ["p", "inclination_rad", "raan_rad"])
Satellite = namedtuple("Satellite", ["plane", "n", "arglat_rad"])


@attr.s(auto_attribs=True)
class Constellation:
    satellites: [Satellite]

    @property
    def num_satellites(self):
        return len(self.satellites)

    @property
    def num_planes(self):
        return max(sat.plane.p for sat in self.satellites) + 1

    def plot(self, plotter: OrbitPlotter3D | None = None):
        """Plots constellation."""
        if plotter is None:
            plotter = OrbitPlotter3D()

        for satellite in self.satellites:
            label = f"n = {satellite.n}, p = {satellite.plane.p}"
            orbit = orbit_from_satellite(satellite)
            plotter.plot(
                orbit,
                color="#000",
                label=label,
            )

        return plotter


def orbit_from_satellite(satellite: Satellite, epoch=None):
    if epoch is None:
        epoch = Time.now()

    orbit = Orbit.from_classical(
        Earth,
        Earth.R * 1.1,
        0.0 * u.one,
        satellite.plane.inclination_rad * u.rad,
        satellite.plane.raan_rad * u.rad,
        0.0 * u.rad,
        satellite.arglat_rad * u.rad,
        epoch=epoch,
    )
    return orbit
