"""
Constellation designer library.
"""

from ._version import __version__
from .core import Constellation
from .manager import ConstellationManager

__all__ = ["__version__", "Constellation", "ConstellationManager"]
