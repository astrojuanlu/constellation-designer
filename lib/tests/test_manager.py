import datetime as dt

from constellation_designer.manager import generate_basename


def test_generate_basename_returns_expected_result(constellation):
    date = dt.datetime(2021, 6, 28, 11, 0, 1)
    expected_basename = "constellation_15_3_omm_2021-06-28T11_00_01"

    basename = generate_basename(constellation, "omm", date)

    assert basename == expected_basename
