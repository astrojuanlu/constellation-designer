from math import pi

import pytest

from constellation_designer.core import Constellation, Plane, Satellite


@pytest.fixture
def constellation():
    return Constellation(
        [
            Satellite(Plane(p, 0, p * 2 * pi), n, n * 2 * pi)
            for p in range(3)
            for n in range(5)
        ]
    )
