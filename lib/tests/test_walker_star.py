import numpy as np
import pytest

from constellation_designer.walker import walker_star


# Table 1
@pytest.mark.parametrize(
    "p,n,expected_d_max,expected_α,expected_β",
    [
        (2, 3, 66.7, 52.2, 37.8),
        (2, 4, 57.0, 48.4, 41.6),
        (2, 5, 53.2, 47.7, 42.3),
        (3, 4, 48.6, 34.7, 20.7),
        (3, 5, 42.1, 32.8, 24.5),
        (3, 6, 38.7, 32.2, 25.7),
        (3, 7, 36.3, 31.4, 27.2),
        (4, 6, 33.5, 24.7, 16.0),
    ],
)
def test_walker_star(p, n, expected_d_max, expected_α, expected_β):
    d_max, α, β = walker_star(n, p)

    assert np.degrees(d_max) == pytest.approx(expected_d_max, abs=1e-1)
    assert np.degrees(α) == pytest.approx(expected_α, abs=1e-0)
    assert np.degrees(β) == pytest.approx(expected_β, abs=1e-0)
