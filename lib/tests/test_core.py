def test_num_planes_and_satellites_returns_correct_result(constellation):
    expected_num_planes = 3
    expected_num_satellites = 3 * 5

    num_planes = constellation.num_planes
    num_satellites = constellation.num_satellites

    assert num_planes == expected_num_planes
    assert num_satellites == expected_num_satellites
