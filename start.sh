#!/usr/bin/env bash
# Start Constellation Designer

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

python -m voila \
  --port "${PORT:-8080}" \
  --Voila.ip="0.0.0.0" \
  --VoilaConfiguration.file_whitelist="['output/.*\.(xml)']" \
  "Constellation Designer.ipynb"
