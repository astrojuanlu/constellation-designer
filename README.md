# Constellation Designer

Web application to design satellite constellations
according to different orbital patterns
for the [OpenSatCom initiative](https://opensatcom.org/)
by European Space Agency and Libre Space Foundation.

A test deployment can be found at https://constellation-designer.herokuapp.com/.

## Installation

To install the dependencies:

```
(env) $ python -m pip install -r requirements.txt
```

or, alternatively, with [pip-tools](https://pypi.org/project/pip-tools/):

```
(env) $ pip-sync
```

## Running

To launch Constellation Designer, use `start.sh`:

```
(env) $ ./start.sh
```

or, alternatively, run `voila` with the appropriate command line options
(see script for customization hints).

## Development

To install the development dependencies as well:

```
(env) $ pip-sync requirements.txt dev-requirements.txt
```

And to run the tests:

```
(env) $ PYTHONPATH=$(pwd) pytest
```

## References

A mind map of relevant references is available under `references/`.
It can be edited with [Heimer](https://github.com/juzzlin/Heimer/) [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)
and a PNG preview is also included.

This is a list of the most important ones:

- Walker, J. G. (1970). _Circular orbit patterns providing continuous whole earth coverage_. Royal Aircraft Establishment Farnborough (United Kingdom).
- Walker, J. G. (1977). _Continuous whole-earth coverage by circular-orbit satellite patterns_. Royal Aircraft Establishment Farnborough (United Kingdom).
- Morrison, J. J. (1973). _A system of sixteen synchronous satellites for worldwide navigation and surveillance_ (No. DOT-TSC-FAA-72-31). United States. Federal Aviation Administration.
- van der Ha, J. C. (Ed.). (2012). _Mission Design & Implementation of Satellite Constellations: Proceedings of an International Workshop, Held in Toulouse, France, November 1997_ (Vol. 1). Springer Science & Business Media.
- Dai, G., Chen, X., Wang, M., Fernández, E., Nguyen, T. N., & Reinelt, G. (2017). Analysis of satellite constellations for the continuous coverage of ground regions. _Journal of Spacecraft and Rockets, 54_(6), 1294-1303.
- Beech, T. W., Cornara, S., Mora, M. B., & Lecohier, G. D. (1999, February). A study of three satellite constellation design algorithms. In _14th international symposium on space flight dynamics_.
- Lee, H. W., Shimizu, S., Yoshikawa, S., & Ho, K. (2020). Satellite Constellation Pattern Optimization for Complex Regional Coverage. _Journal of Spacecraft and Rockets, 57_(6), 1309-1327.
- Mortari, D., Wilkins, M. P., & Bruccoleri, C. (2004). The flower constellations. _The Journal of the Astronautical Sciences, 52_(1), 107-127.
